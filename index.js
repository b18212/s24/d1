// ES6 Updates
// Exponent Operator (**)

console.log(">>>--- Exponent Operator---<<<");
// Exponent Operator (**)
const firstNum = 8 ** 2;
console.log(firstNum);
console.log(" ");
console.log(" ");
// result: 64


// before ES6 Update
console.log(">>>--- before ES6 Update---<<<");
const secondNum = Math.pow(8, 2)
console.log(secondNum);
console.log(" ");
console.log(" ");

/*
	Mini-Activity:
	>> Using exponent operator get the cube of 5
	>> Store the result in a variable called getCube
	>> Print the result in the console browser
	>> Send your outputs in Hangout


*/

// SOLUTION
console.log(">>>--- Get Cube---<<<");
let getCube = 5 ** 3
console.log(getCube);
console.log(" ");
console.log(" ");

// Template Literals
console.log(">>>--- Template Literals---<<<");
let name = "Tine";


// before the update
let message = ('Hello ' + name + '! ' + 'Welcome to programming');
console.log('Result from pre-template literals:')
console.log(message);
console.log(" ");
console.log(" ");

// ES6 updates
//  this uses backticks
console.log(">>>--- Template Literals / ES6 updates---<<<")
message = (`Hello ${name}! Welcome to programming.`)
console.log('Result with template literals:')
console.log(message);
console.log(" ");
console.log(" ");


/*
	Mini-Activy:
		>> Create a sentence using the variables given below
		>> Send a screenshot output in Hangouts

*/
let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "Javascript";
let string6 = "as a";
let string7 = "programming language";
message1 = (`${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7}`)
console.log(message1);
console.log(" ");
console.log(" ");

const interestRate = .1;
const principal = 1000;

console.log(`The interest in your savings account is: ${principal *interestRate}`);
//result: 100
console.log(" ");
console.log(" ");

// Array Destructuring
console.log(">>>--- Array Destructuring---<<<")
/*
	Syntax:
		let / const [variableNameA, variableNameB, . . .] = arrayNAme

*/

const fullName = ["Juan", "Dela", "Cruz"];

// before update
console.log(">>>--- Array Destructuring Before Update---<<<")
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

console.log(" ");
console.log(" ");

// es6Updates
console.log(">>>--- Array Destructuring ES6 Update---<<<")
const [firstName, middleName, lastName] = fullName;
// if you are going to skip
// const [, middleName, ] = fullName;
// assigning only a middle name
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ! Nice to meet yo`);

console.log(" ");
console.log(" ");

console.log(">>>--- Array Destructuring ES6 Update---<<<")

// Mini Activuty
/* >> Desctructure the given array and save element in the ff variables
	
*/

let pointGuards = ['Curry', 'Lillard', 'Paul', 'Irving'];

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = pointGuards;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);
console.log(" ");
console.log(" ");

// Object Destructure
/*
	Syntax:
		let / const {propertyNameA, propertyB, ...} = objectName

*/

// before updates

console.log(">>>--- Object Destructure before Updates---<<<")
const person = {
    giveName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz'
}

console.log(person.giveName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.giveName} ${person.maidenName} ${person.familyName}! It was nice meeting you`)
console.log(" ");
console.log(" ");

// ES6 Updates
// note that order does not matter in object desctructuring
console.log(">>>--- Object Destructure ES6 Updates ---<<<")
const {giveName, maidenName, familyName} = person;

console.log(giveName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${giveName} ${maidenName} ${familyName}! It was nice meeting you`)


console.log(" ");
console.log(" ");

/*

Mini Activity
	>> Given the object below, destructure each property
	>> Create a sentence variable with the ff. string message:
		My Pokemon is <pokemon1Name>, it is level <levelOfPokemon>. It is a <typeOfPokemon> type.

*/
let pokemon1 = {
	charterName: "Charmander",
	level: 11,
	type: "Fire"
}

// SOLUTION ES6
const {charterName, level, type} = pokemon1;
let sentence2 = `My Pokemon is ${charterName}, it is level ${level}. It is a ${type} type.`
console.log(sentence2);
console.log(" ");
console.log(" ");

// destructuring in function parameter
console.log(">>>--- destructuring in function parameter Updates ---<<<")
function getFullName({giveName, maidenName, familyName}){
	console.log(`${giveName} ${maidenName} ${familyName}`)
};

getFullName(person);

console.log(" ");
console.log(" ");


// Arrow Function

/*
	Syntax:
		cost variableName = (parameter) => {
			statement / code block
		}

*/ 

console.log(">>>--- Arrow Function before update---<<<")
console.log("It was comment out here check the codes!")
// function printFullName(firstName, middleName, lastName){
// 	console.log(`${firstName} ${maidenName} ${lastName}`)
// };

// printFullName('John', 'D', 'Smith');

console.log(" ");
console.log(" ");


//ES6 Update
console.log(">>>--- Arrow Function ES6 update---<<<")
const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${maidenName} ${lastName}`);
};

printFullName('John', 'D', 'Smith');

console.log(" ");
console.log(" ");


const students = ['John', 'Jane', 'Juan'];

// forEach with traditional function
// student.forEach(function(){

// });

// arrow function
students.forEach(student => {
	console.log(`${student} is a student`)
});

students.forEach(student => console.log(`${student} is a student`));

console.log(" ");
console.log(" ");



// implicit return
// one liner function
console.log(">>>--- Implicit return---<<<")
const add = (x, y) => x + y
let total = add(27, 9);
console.log(total);
// result

console.log(" ");
console.log(" ");


// explicit return


console.log(">>>--- Explicit return---<<<")
console.log(">>>--- Comment out check the codes here!---<<<")
// const add = (x, y) => {
// 	return x + y
// };

// let total = add(9, 18);
// console.log(total);
console.log(" ");
console.log(" ");

// Default function argument value
console.log(">>>--- Default function argument value ---<<<")
const greet = (name = 'User') => {
	return `Good day! ${name}.`
};
console.log(greet());
console.log(" ");
console.log(" ");


// traditional function
console.log(">>>--- traditional function ---<<<")
console.log(">>>--- Comment out check the codes here!---<<<")
// function greet (name = 'User'){
// 	return `Good day! ${name}`
// }

console.log(" ");
console.log(" ");

console.log(">>>--- Class-Based Object Blueprints ---<<<")
// Class-Based Object Blueprints
/*
	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Syntax:
	// let /  const variableName = new className()

	let myCar = new Car();

	// const myCar = new Car();
	console.log(myCar);
	// result: undefined

	myCar.brand = 'Ford';
	myCar.name = 'Ranger Raptor';
	myCar.year = 2021;

	console.log(myCar);
	// crreate a new object from the car class with initialized value
	const newCar = new Car("Toyota", "Vios", 2021);
	console.log(newCar);


